<?php

namespace Controller;

use Powerup\Model;

class Site extends \Powerup\Controller\Site {

    function home() {

        $data['test'] = (new Model\Core)->fetchAll();

        return ['view' => '/pages/home', 'data' => $data];

    }

	function about() {

		return ['view' => '/pages/about'];

	}

}

?>