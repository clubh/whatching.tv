<?php

namespace Controller;

use Powerup\Model;

class Admin extends \Powerup\Controller\Admin {

	function dashboard() {

        $data = $this->data;

        $data['simple_list'] = ['Peter','Paul','Mary'];

        $data['multi_list'] = [0 => 'aa', 1 => 'bb', 2 => 'cc'];

		return ['view' => '/pages/admin/dashboard', 'data' => $data];

	}

}

?>