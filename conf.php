<?php

// Directories

define('ROOT', __DIR__);
define('POWERUPROOT', __DIR__.'/powerup');
define('WEBROOT', $_SERVER['DOCUMENT_ROOT']);
define('EOL', PHP_EOL);

// Locales

setlocale(LC_ALL, 'en_US.UTF8');
date_default_timezone_set('Europe/Copenhagen');

// Define environment

$_ENV['conf'] = [
	'database' => [
		'host' => 'localhost',
		'username' => 'root',
		'password' => 'root',
		'name' => 'powerup',
	],
    'mail' => [
    ],
	'keys' => [
        'auth' => [
            'google' => '/keys/google.auth.json',
            'facebook' => '/keys/facebook.auth.json'
        ]
	],
	'localhosts' => ['127.0.0.1', '::1']
];

// Define page variables

$data['app']['locales']['en'] = 'English';

// ENV

$data['env']['locale'] = 'en';

$data['google']['tagManager']['id'] = 'UA-155718-16';

// META

$data['meta']['title'] = 'PowerUp - A Fullstack PHP Framework for Super Heroes';

$data['meta']['year'] = date('Y');

?>