<?php

return [

    ## Site

    '[/]' => ['Controller\Site', 'home', 'GET'],
    '/about' => ['Controller\Site', 'about', 'GET'],

    ## Auth

    '/login' => ['Powerup\Controller\Auth', 'login', 'GET'],
    '/login/{vendor:google|facebook|twitter}' => ['Powerup\Controller\Auth', 'vendor', 'GET'],
    '/login/{vendor:google|facebook|twitter}/callback' => ['Powerup\Controller\Auth', 'callback', 'GET'],
    '/logout' => ['Powerup\Controller\Auth', 'logout', 'GET'],

    ## Admin

    '/admin' => ['Controller\Admin', 'dashboard', 'GET'],
    '/admin/user/update' => ['Powerup\Controller\User', 'update', 'POST'],

    ## Panel

    '/panel' => ['Controller\Admin', 'dashboard', 'GET'],

    ## API

    '/api' => ['Powerup\Controller\Api', 'getAllUsers', 'GET'],
    '/api/user' => ['Powerup\Controller\Api', 'getUser', 'GET'],
    '/api/me' => ['Powerup\Controller\Api', 'getMe', 'GET'],

    ## Adding route

    '/test' => ['Powerup\Controller\Test', 'test', 'GET'],

];

?>