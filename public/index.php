<?php

// Get configuration

try {

    if (! @include_once '../conf.php') {

        throw new Exception ();

    }

} catch (Exception $e) {

    dd(['Powerup' => 'Missing configuration file']);

}

// Get global functions

include_once POWERUPROOT.'/lib/system.php';

// Now we powerup

include_once POWERUPROOT.'/init.php';

?>